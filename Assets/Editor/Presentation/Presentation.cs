﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class PresentationWindow : EditorWindow {

    static string inputFileName;
    bool init = true;
    SlideManager slideManager;

    [MenuItem("Presentaion/Open")]
    static void Open() {
        inputFileName = EditorUtility.OpenFilePanelWithFilters("スライドを開く", "./", new[] { "unityslide file", "Unityslide,unityslide", "All files", "*" }) ;
        EditorWindow.GetWindow<PresentationWindow>(System.IO.Path.GetFileName(inputFileName));
    }

    private void OnGUI() {
        if (init) {
            slideManager = new SlideManager(inputFileName);
            maxSize = new Vector2(slideManager.width, slideManager.height);
            minSize = maxSize;
            init = false;
        }
        slideManager.Draw();

        if (Event.current.keyCode == KeyCode.Return && Event.current.type == EventType.KeyDown) {
            slideManager.MoveToNext();
        }

        if(Event.current.keyCode == KeyCode.Backspace && Event.current.type == EventType.KeyDown) {
            slideManager.MoveToBack();
        }

        if (slideManager.finished && Event.current.keyCode == KeyCode.Return && Event.current.type == EventType.KeyDown) {
            Close();
        }
        if(Event.current.keyCode == KeyCode.Escape && Event.current.type == EventType.KeyDown) {
            Close();
        }
    }

    private void Update() {
        Repaint();
    }
}
