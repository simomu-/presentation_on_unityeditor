﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[SerializeField]
public class PresentationSlides {
    public List<Slide> slides;
    public int width;
    public int height;

    Texture2D texture;

    public void DrawBackground() {
        if(texture == null) {
            texture = new Texture2D(1, 1);
            texture.LoadImage(System.IO.File.ReadAllBytes("Assets/Editor/Presentation/Background.png"));
            texture.filterMode = FilterMode.Point;
            texture.alphaIsTransparency = false;
        }

        EditorGUI.DrawPreviewTexture(new Rect(0, 0, width, height),
                            texture,
                            null);
    }
}

[System.Serializable]
public class Slide {
    public string title;
    public List<ContentText> contentTexts;
    public List<ContentImage> contentImaes;
    public List<FreePositionText> freePositionTexts;
}

[System.Serializable]
public class ContentText {
    public string text;
    public List<ContentText> childContentTexts;
}

[System.Serializable]
public class ContentImage {
    public string filePath;
    public Vector2 position;
    public Vector2 scale;

    Texture2D texture;
    public void LoadTexture() {
        texture = new Texture2D(1, 1);
        texture.LoadImage(System.IO.File.ReadAllBytes(filePath));
        texture.filterMode = FilterMode.Point;
        texture.alphaIsTransparency = false;
    }

    public void DrawTexture() {
        //EditorGUI.DrawPreviewTexture(new Rect(position.x - scale.x / 2f, position.y - scale.y / 2f, scale.x,scale.y), texture, null, ScaleMode.ScaleToFit);
        EditorGUI.DrawPreviewTexture(new Rect(position.x - texture.width * scale.x / 2f,
                                              position.y - texture.height * scale.y / 2f, 
                                              texture.width * scale.x,
                                              texture.height * scale.y), 
                                    texture, 
                                    null, 
                                    ScaleMode.ScaleToFit);
    }
}

[System.Serializable]
public class FreePositionText {
    public string text;
    public Vector2 position;
}
