﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SlideManager {

    static bool generateTemplete = false;

    public bool finished = false;

    PresentationSlides presentation;
    int currentSlideIndex = 0;

    public int width {
        get {
            return presentation.width;
        }
    }

    public int height {
        get {
            return presentation.height;
        }
    }

    public SlideManager(string filePath) {
        if (generateTemplete) {
            PresentationSlides slide = new PresentationSlides();
            slide.width = 800;
            slide.height = 480;
            slide.slides = new List<Slide>();
            slide.slides.Add(new Slide());
            slide.slides[0].title = "Slide Title";
            slide.slides[0].contentTexts = new List<ContentText>();
            slide.slides[0].contentTexts.Add(new ContentText());
            slide.slides[0].contentTexts[0].text = "text";
            slide.slides[0].contentTexts[0].childContentTexts = new List<ContentText>();
            slide.slides[0].contentTexts[0].childContentTexts.Add(new ContentText());
            slide.slides[0].contentTexts[0].childContentTexts[0].text = "sub text";
            slide.slides[0].contentTexts[0].childContentTexts[0].childContentTexts = new List<ContentText>();
            slide.slides[0].contentTexts[0].childContentTexts[0].childContentTexts.Add(new ContentText());
            slide.slides[0].contentTexts[0].childContentTexts[0].childContentTexts[0].text = "sub sub text";

            slide.slides[0].freePositionTexts = new List<FreePositionText>();
            slide.slides[0].freePositionTexts.Add(new FreePositionText());
            slide.slides[0].freePositionTexts[0].text = "free position text";
            slide.slides[0].freePositionTexts[0].position = new Vector2(200, 200);

            slide.slides[0].contentImaes = new List<ContentImage>();
            slide.slides[0].contentImaes.Add(new ContentImage());
            slide.slides[0].contentImaes[0].filePath = "test.jpg";
            slide.slides[0].contentImaes[0].position = new Vector2(400, 350);
            slide.slides[0].contentImaes[0].scale = Vector2.one * 0.3f;

            System.IO.File.WriteAllText("templete.unityslide", JsonUtility.ToJson(slide, true));
        }
        presentation = JsonUtility.FromJson<PresentationSlides>(System.IO.File.ReadAllText(filePath));
        foreach(var slide in presentation.slides) {
            foreach(var image in slide.contentImaes) {
                image.LoadTexture();
            }
        }

        Debug.Log("window size " + presentation.width + "," + presentation.height);
        Debug.Log("slide count " + presentation.slides.Count);
    }

    public void Draw() {

        presentation.DrawBackground();

        GUILayout.Space(20);
        GUIStyle tStyle = new GUIStyle();
        tStyle.font = EditorStyles.boldFont;
        tStyle.fontSize = 50;
        GUILayout.Label(presentation.slides[currentSlideIndex].title, tStyle);
        GUILayout.Space(20);

        DrawContentText(presentation.slides[currentSlideIndex].contentTexts, 0);

        foreach(var image in presentation.slides[currentSlideIndex].contentImaes) {
            image.DrawTexture();
        }
        foreach (var freeText in presentation.slides[currentSlideIndex].freePositionTexts) {
            GUIStyle style = new GUIStyle();
            style.font = EditorStyles.boldFont;
            style.fontSize = 40;
            EditorGUI.LabelField(new Rect(freeText.position,Vector2.one), freeText.text, style);
        }

    }

    public void MoveToNext() {
        if (currentSlideIndex + 1 < presentation.slides.Count) {
            currentSlideIndex++;
        } else {
            finished = true;
        }
    }

    public void MoveToBack() {
        if(currentSlideIndex - 1 >= 0) {
            currentSlideIndex--;
        }
    }

    void DrawContentText(List<ContentText> contentTexts, int hierarchy) {
        
        foreach(var contentText in contentTexts) {
            GUIStyle style = new GUIStyle();
            style.font = EditorStyles.standardFont;
            style.fontSize = 30 - hierarchy * 5;
            string space = "";
            for (int i = 0; i <= hierarchy; i++) {
                space += "  ";
            }
            space += "・";
            GUILayout.Label(space + contentText.text, style);
            if (contentText.childContentTexts.Count != 0) {
                GUILayout.Space(10);
                DrawContentText(contentText.childContentTexts, hierarchy + 1);
            }
            GUILayout.Space(20);
        }
    }
}